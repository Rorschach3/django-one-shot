from django.shortcuts import render
from todos.models import TodoItem, TodoList

# Create your views here.
def Todos(request):
    return render(request, "todos/todos.html")
